import React from 'react';
import logo from './logo.svg';
import SiteHeader from './partials/SiteHeader';
import SiteFooter from './partials/SiteFooter';
import Sidebar from './partials/Sidebar';
import Dashboard from './pages/Dashboard';
import Dashboard2 from './pages/Dashboard2';
import {
	BrowserRouter as Router,
	Route,
	Link,
	Switch
} from 'react-router-dom';
import './App.css';

function App() {
	return (
	  	<Router >
		    <div className="App">
				<SiteHeader />
				<Sidebar />
				<Switch>
					<Route path="/dashboard" exact>
						<Dashboard />
					</Route>
					<Route path="/dashboard2" exact>
						<Dashboard2 />
					</Route>
					<Route path="/" exact>
						<Dashboard />
					</Route>
				</Switch>
				<SiteFooter />
		    </div>
	    </Router>
	);
}

export default App;
